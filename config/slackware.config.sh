export PATH="$HOME/.local/bin:/usr/sbin:$PATH"

# Case Insensitive for auto completion.
bind 'set completion-ignore-case On'

# Force use EDITOR with vim
export EDITOR=vim

# Setup less for git
export LESS=-MR

# Setup vim mode
set -o vi

# Show vi mode for bash
bind 'set show-mode-in-prompt on'
bind 'set keyseq-timeout 1'
bind 'TAB:menu-complete'
bind '"\e[Z":menu-complete-backward'
bind 'set show-all-if-ambiguous on'
bind 'set completion-ignore-case on'
bind 'set completion-prefix-display-length 3'
bind 'set menu-complete-display-prefix on'
