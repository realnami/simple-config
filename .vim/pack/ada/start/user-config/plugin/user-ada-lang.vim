function! Ada_loading()
  set makeprg=make
  set cscopeprg=gtags-cscope

"  if !exists("enable_ada_language_loading")
    noremap gs] :sts<CR>

    let enable_ada_language_loading = 1

    let g:tagbar_type_ada = {
          \ 'ctagstype': 'ada',
          \ 'kinds' : [
          \'P:package specs',
          \'p:packages',
          \'t:type',
          \'u:subtypes',
          \'c:record type components',
          \'l:enum type literals',
          \'v:variables',
          \'f:formal parameters',
          \'n:constants',
          \'x:exceptions',
          \'R:subprogram specs',
          \'r:subprograms',
          \'K:task specs',
          \'k:tasks',
          \'O:protected data specs',
          \'o:protected data',
          \'e:entries',
          \'b:labels',
          \'i:identifiers'
          \]
          \}

    " use tagbar for Ada language
    nmap <F1> :TagbarToggle<CR>

    nmap <F5> :w<CR>:make<CR><CR><CR>

    packadd tagbar
"  endif
endfunction

autocmd FileType ada call Ada_loading()
autocmd BufRead,BufNewFile *.ads,*.adb call Ada_loading()
