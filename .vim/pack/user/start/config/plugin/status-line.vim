set laststatus=2

set statusline=%{StatuslineMode()}\ %1*%m%*\ (%l/%L)%=%y\ <<%.20f>>
au InsertEnter * call InsertModeColor()

function InsertModeColor()
  hi User1 term=inverse cterm=inverse ctermfg=red
endfunction

function! StatuslineMode()
  let l:mode=mode()
  if l:mode==#"n"
    return "NORMAL"
  elseif l:mode==?"v"
    return "VISUAL"
  elseif l:mode==#"i"
    return "INSERT"
  elseif l:mode==#"R"
    return "REPLACE"
  elseif l:mode==?"s"
    return "SELECT"
  elseif l:mode==#"t"
    return "TERMINAL"
  elseif l:mode==#"c"
    return "COMMAND"
  elseif l:mode==#"!"
    return "SHELL"
  endif
endfunction
