#!/usr/bin/env bash

ENABLE_PATH=$SIMPLE_CONFIG/enable

for item in $(ls $ENABLE_PATH); do
  . $ENABLE_PATH/$item
done
